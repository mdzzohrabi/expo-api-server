'use strict';

/**
 * Contentmedia.js controller
 *
 * @description: A set of functions called "actions" for managing `Contentmedia`.
 */

module.exports = {

  /**
   * Retrieve contentmedia records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.contentmedia.search(ctx.query);
    } else {
      return strapi.services.contentmedia.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a contentmedia record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.contentmedia.fetch(ctx.params);
  },

  /**
   * Count contentmedia records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.contentmedia.count(ctx.query);
  },

  /**
   * Create a/an contentmedia record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.contentmedia.add(ctx.request.body);
  },

  /**
   * Update a/an contentmedia record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.contentmedia.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an contentmedia record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.contentmedia.remove(ctx.params);
  }
};
