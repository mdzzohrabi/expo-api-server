'use strict';

/**
 * Stats.js controller
 *
 * @description: A set of functions called "actions" for managing `Stats`.
 */

module.exports = {

  /**
   * Retrieve stats records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.stats.search(ctx.query);
    } else {
      return strapi.services.stats.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a stats record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.stats.fetch(ctx.params);
  },

  /**
   * Count stats records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.stats.count(ctx.query);
  },

  /**
   * Create a/an stats record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.stats.add(ctx.request.body);
  },

  /**
   * Update a/an stats record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.stats.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an stats record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.stats.remove(ctx.params);
  }
};
