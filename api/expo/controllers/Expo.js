'use strict';

/**
 * Expo.js controller
 *
 * @description: A set of functions called "actions" for managing `Expo`.
 */

module.exports = {

  /**
   * Retrieve expo records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.expo.search(ctx.query);
    } else {
      return strapi.services.expo.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a expo record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.expo.fetch(ctx.params);
  },

  /**
   * Count expo records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.expo.count(ctx.query);
  },

  /**
   * Create a/an expo record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.expo.add(ctx.request.body);
  },

  /**
   * Update a/an expo record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.expo.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an expo record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.expo.remove(ctx.params);
  }
};
