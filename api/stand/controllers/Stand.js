'use strict';

/**
 * Stand.js controller
 *
 * @description: A set of functions called "actions" for managing `Stand`.
 */

module.exports = {

  /**
   * Retrieve stand records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.stand.search(ctx.query);
    } else {
      return strapi.services.stand.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a stand record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.stand.fetch(ctx.params);
  },

  /**
   * Count stand records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.stand.count(ctx.query);
  },

  /**
   * Create a/an stand record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.stand.add(ctx.request.body);
  },

  /**
   * Update a/an stand record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.stand.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an stand record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.stand.remove(ctx.params);
  }
};
