'use strict';

/**
 * Acl.js controller
 *
 * @description: A set of functions called "actions" for managing `Acl`.
 */

module.exports = {

  /**
   * Retrieve acl records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.acl.search(ctx.query);
    } else {
      return strapi.services.acl.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a acl record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.acl.fetch(ctx.params);
  },

  /**
   * Count acl records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.acl.count(ctx.query);
  },

  /**
   * Create a/an acl record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.acl.add(ctx.request.body);
  },

  /**
   * Update a/an acl record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.acl.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an acl record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.acl.remove(ctx.params);
  }
};
